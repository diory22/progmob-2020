package com.example.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        EditText edCariNidn = (EditText)findViewById(R.id.editTextCariNidn);
        final EditText edNamaBaruDsn = (EditText)findViewById(R.id.editTextNamaBaruDsn);
        final EditText edNIDNBaru = (EditText)findViewById(R.id.editTextNIDNBaru);
        final EditText edAlamatBaruDsn = (EditText)findViewById(R.id.editTextAlamatBaruDsn);
        final EditText edEmailBaruDsn = (EditText)findViewById(R.id.editTextEmailBaruDsn);
        final EditText edGelarBaru = (EditText)findViewById(R.id.editTextGelarBaru);
        Button btnUbahDsn = (Button)findViewById(R.id.buttonUbahDsn);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        btnUbahDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dsn(
                        edNamaBaruDsn.getText().toString(),
                        edNIDNBaru.getText().toString(),
                        edAlamatBaruDsn.getText().toString(),
                        edEmailBaruDsn.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "Diory Panjaitan",
                        "72180266"
                );
                call.enqueue(new Callback<DefaultResult>() {

                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this,"DATA BERHASIL DIUBAH",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this,"GAGAL",Toast.LENGTH_LONG).show();
                    }
                }
                );
            }
        });
    }
}