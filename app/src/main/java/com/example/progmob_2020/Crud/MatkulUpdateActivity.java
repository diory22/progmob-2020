package com.example.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        EditText edCariKode = (EditText)findViewById(R.id.editTextCariKode);
        final EditText edNamaBaruMatkul = (EditText)findViewById(R.id.editTextNamaBaruMatkul);
        final EditText edKodeBaru = (EditText)findViewById(R.id.editTextKodeBaru);
        final EditText edHariBaru = (EditText)findViewById(R.id.editTextHariBaru);
        final EditText edSesiBaru = (EditText)findViewById(R.id.editTextSesiBaru);
        final EditText edSksBaru = (EditText)findViewById(R.id.editTextSksBaru);
        Button btnUbahMatkul = (Button)findViewById(R.id.buttonUbahMatkul);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        btnUbahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edNamaBaruMatkul.getText().toString(),
                        edKodeBaru.getText().toString(),
                        edHariBaru.getText().toString(),
                        edSesiBaru.getText().toString(),
                        edSksBaru.getText().toString(),
                        "72180266"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"DATA BERHASIL DIUBAH",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"GAGAL",Toast.LENGTH_LONG).show();
                    }
                }
                );
            }
        });
    }
}