package com.example.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText edNamaDsn = (EditText)findViewById(R.id.editTextNamaDsn);
        final EditText edNidn = (EditText)findViewById(R.id.editTextNidn);
        final EditText edAlamatDsn = (EditText)findViewById(R.id.editTextAlamatDsn);
        final EditText edEmailDsn = (EditText)findViewById(R.id.editTextEmailDsn);
        final EditText edGelar = (EditText)findViewById(R.id.editTextGelar);
        Button btnSimpanDsn = (Button)findViewById(R.id.buttonSimpanDsn);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnSimpanDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        edNamaDsn.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamatDsn.getText().toString(),
                        edEmailDsn.getText().toString(),
                        edGelar.getText().toString(),
                        "Diory Panjaitan",
                        "72180266"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this,"DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this,"GAGAL",Toast.LENGTH_LONG).show();
            }
                });
            }
        });
    }
}