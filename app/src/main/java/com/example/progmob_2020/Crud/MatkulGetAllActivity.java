package com.example.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.progmob_2020.Adapter.DosenCRUDRecyclerAdapter;
import com.example.progmob_2020.Adapter.MatkulCRUDRecyclerAdapter;
import com.example.progmob_2020.Model.Dosen;
import com.example.progmob_2020.Model.Matkul;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulGetAllActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMatkul = (RecyclerView)findViewById(R.id.rvGetMatkulAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180266");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulGetAllActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulGetAllActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }
}