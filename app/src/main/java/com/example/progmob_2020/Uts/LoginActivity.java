package com.example.progmob_2020.Uts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.progmob_2020.Crud.DosenGetAllActivity;
import com.example.progmob_2020.Crud.MahasiswaGetAllActivity;
import com.example.progmob_2020.Crud.MainDsnActivity;
import com.example.progmob_2020.Crud.MainMhsActivity;
import com.example.progmob_2020.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        EditText edUsername = (EditText) findViewById(R.id.editTextUserName);
        EditText edPassword = (EditText)findViewById(R.id.editTextPassword);
        Button btnLogin = (Button)findViewById(R.id.buttonLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);

            }
        });
    }
}