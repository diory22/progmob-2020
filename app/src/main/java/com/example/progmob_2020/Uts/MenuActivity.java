package com.example.progmob_2020.Uts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob_2020.Crud.MainDsnActivity;
import com.example.progmob_2020.Crud.MainMatkulActivity;
import com.example.progmob_2020.Crud.MainMhsActivity;
import com.example.progmob_2020.R;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button btnMahasiswa = (Button)findViewById(R.id.buttonMahasiswa);
        Button btnDosen = (Button)findViewById(R.id.buttonDosen);
        Button btnMatkul = (Button)findViewById(R.id.buttonMatkul);

        btnMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });
        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });
        btnMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }

}