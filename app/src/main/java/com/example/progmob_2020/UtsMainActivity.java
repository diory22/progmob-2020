package com.example.progmob_2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.progmob_2020.Crud.MainMatkulActivity;
import com.example.progmob_2020.Crud.MatkulGetAllActivity;
import com.example.progmob_2020.Pertemuan6.PrefActivity;
import com.example.progmob_2020.R;
import com.example.progmob_2020.Uts.LoginActivity;

public class UtsMainActivity extends AppCompatActivity {
    private int waktu_loading=4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(UtsMainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        },waktu_loading);
    }
}